# Authors: Brigham Diaz and Michael Banks
class Todo < ActiveRecord::Base
    attr_accessible :todo_item, :userEmail
    validates :todo_item, presence: true
end
