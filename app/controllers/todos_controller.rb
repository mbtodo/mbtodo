class TodosController < ApplicationController
  before_filter :authenticate_user!
  # index action initializes the todo_array, new todo and sends page to view
  def index
    @todo_array = Todo.where(:todos => { :userEmail => current_user.email })
    @new_todo = Todo.new
    render :index
  end

  # retrieves the todo object that passed from the edit link
  def edit
    @todo = Todo.find(params[:id])
  end

  # get the last item in Todo list and delete it
  def delete
    t = Todo.last
    t.delete
  end

  # updates the task that user has edited
  def update
    @todo = Todo.find(params[:id])
    # security feature - verifies that todo item variable is being passed
    if @todo.update(params.require(:todo).permit(:todo_item))
      flash[:success] = "Todo edited successfully"
      redirect_to :action => 'index'
    else
      render 'edit'
    end
  end

  # create a new todo item
  def add
    todo = Todo.create(:todo_item => params[:todo][:todo_item], :userEmail => current_user.email)
    # use model validation to verify textbox is not empty
    if !todo.valid?
        # If the form was empty we show an error message
        flash[:error] = todo.errors.full_messages.join("<br>").html_safe
    else
        # Otherwise we add the todos item and display a success message
        flash[:success] = "Todo added successfully"

    end
    redirect_to :action => 'index'
  end

  # mark todo item as complete
  def complete
    if params[:todos_checkbox].nil?
      flash[:error] = "You must check at least one todo item to complete"
    else
      # checkbox id will come here as an array so if items 1 and 2 are checked it will be [1, 2]
      params[:todos_checkbox].each do |check|
        todo_id = check
        t = Todo.find_by_id(todo_id)
        # update the status
        t.update_attribute(:completed, true)
      end
    end
    redirect_to :action => 'index'
  end
end
