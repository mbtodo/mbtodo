require 'test_helper'

class TodosControllerTest < ActionController::TestCase

  #ensure that a valid todo_array instance is assigned
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:todo_array)
  end

  # adds a task and verifies that task array count has increased
  test "should add task" do
    assert_difference('Todo.count') do
    post :add, todo: {todo_item: 'Take out teh trash'}
    end
  end

  # verifies routing to delete action exists
  test "should get delete" do
    get :delete
    assert_response :success
  end

  # initialize a todo object and store inside todo_array
  private

  def initialize_todo
    @todo = todo_array(:one)
  end

end