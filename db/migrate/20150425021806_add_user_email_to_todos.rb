class AddUserEmailToTodos < ActiveRecord::Migration
  def change
    add_column :todos, :userEmail, :string
  end
end
