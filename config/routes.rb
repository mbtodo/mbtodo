# Authors: Brigham Diaz and Michael Banks
Rails.application.routes.draw do
  # The route devise uses to enable login, etc.
  devise_for :users
  # The main page of the application.
  get 'todos/index'

  # Used to display deletion confirmation
  get "todos/delete" => "todos#delete", :as => :delete

  # Used to send the todo id to the edit function
 get "todos/:id/edit" => "todos#edit", :as => :edit
  resources :todo do
    post :edit, on: :member
  end

  # Directs index to add action
  post "todos/add" => "todos#add"

  # Used to tie the complete form to action
  match 'todos/complete' => 'todos#complete', :via => :post

  # Match the todo object to :id
  patch 'todos/:id/update' => 'todos#update', :as => :update

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'todos#index'
end
